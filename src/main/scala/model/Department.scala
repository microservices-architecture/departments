package model

/**
  * Created by amartinm82 on 16/03/2017.
  */
case  class Department (id: Long, name: String)
