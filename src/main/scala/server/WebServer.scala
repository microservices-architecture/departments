package server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import akka.event.Logging
import model.Department

import scala.concurrent.Future
import akka.Done
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Route
import com.typesafe.config.ConfigFactory
import spray.json.DefaultJsonProtocol


import scala.io.StdIn

/**
  * Created by Alvaro Martin on 07/03/2017.
  */
// formats for unmarshalling and marshalling
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val departmentFormat = jsonFormat2(Department)
}

object WebServer extends Directives with JsonSupport{

  def main(args: Array[String]) {

    // needed to run the route
    implicit val system = ActorSystem("my-system")
    implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher

    // logs and settings
    val log = Logging(system, this.getClass)
    val config = ConfigFactory.load()
    val port : Int = config.getInt("departments.port")
    val pathGet : String = config.getString("departments.path.get")
    val pathPost :String = config.getString("departments.path.post")
    log.debug("Settings loaded with values:\npath.get:{}\npath.post:{}\nport:{}",pathGet,pathPost,port)

    // (fake) async database query api
    def fetchDepartment(departmentId: Long): Future[Option[Department]] = Future{Some(Department(1,"Department 1"))}(executionContext)


    val route : Route =
      get {
        pathPrefix(pathGet / LongNumber) { id =>
          // there might be no department for a given id
          val maybeDepartment: Future[Option[Department]] = fetchDepartment(id)

          onSuccess(maybeDepartment) {
            case Some(department) => complete(department)
            case None       => complete(StatusCodes.NotFound)
          }
        }
      } ~
        post {
          path(pathPost) {
            entity(as[Department]) { department => // will unmarshal JSON to Department
              val departmentId = department.id
              val departmentName = department.name
              complete(s"Department $departmentId with name $departmentName created")
            }
          }
        }

    val bindingFuture = Http().bindAndHandle(route, "localhost", port)

    log.info("Server listening at http://localhost:{}/\nPress RETURN to stop...",port)
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
    log.info("Server stopped")
  }
}